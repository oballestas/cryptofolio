from django.contrib import admin
from cryptocoin.models import TransactionType, CryptoCurrency, Transaction

# Register your models here.
admin.site.register(TransactionType)
admin.site.register(CryptoCurrency)
admin.site.register(Transaction)

