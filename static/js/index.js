export async function consumeAPI(endpoint = 'cryptocurrencies/', pk = null) {

    try {

        let BASE_URL = "http://localhost:8000/api/";
        let URL = `${BASE_URL}${endpoint}${pk ? `${pk}/` : ''}`;

        async function fun() {
            const response = await fetch(URL);
            return await response.json();
        }

        return await fun();

    } catch (error) {

        console.log(error);
        return error;

    }

}
