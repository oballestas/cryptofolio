# Generated by Django 4.0.4 on 2022-04-22 14:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cryptocoin', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Portafolio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.FloatField()),
                ('coin_price', models.FloatField()),
                ('cryptocurrency_symbol', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cryptocoin.cryptocurrency')),
                ('transaction_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cryptocoin.transactiontype')),
            ],
        ),
    ]
