from cryptocoin.models import CryptoCurrency
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
import json


# Create your tests here.

class CryptoCurrencyListTest(APITestCase):
    """
    Test for all the cryptocurrencies:
    -For the 'get' method.
    -For the 'post' method for successful and failed responses.
    """

    def setUp(self):
        self.cryptocurrency_data = {
            "name": "TestCoin",
            "symbol": "TC",
            "logo": "https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
            "price": 50000.32,
            "last_day_change": 4.0
        }

    def test_get_cryptocurrencies(self):
        CryptoCurrency.objects.create(**self.cryptocurrency_data)
        response = self.client.get(reverse('cryptocurrencies_list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(len(result), 1)

    def test_create_cryptocurrency_success(self):
        url = reverse('cryptocurrencies_list')
        response = self.client.post(url, self.cryptocurrency_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        result = json.loads(response.content)
        self.assertEqual(result.get('name'), self.cryptocurrency_data.get('name'))

    def test_create_cryptocurrency_fail(self):
        url = reverse('cryptocurrencies_list')
        fail_data = {}
        response = self.client.post(url, fail_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class CryptoCurrencyDetailTest(APITestCase):
    """
    Test for a specific cryptocurrency (detail view):
    -For the 'get' and 'put' methods for successful and failed responses.
    -For the 'delete' method.
    """

    def setUp(self):
        self.cryptocurrency_data = {
            "name": "TestCoin",
            "symbol": "TC",
            "logo": "https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
            "price": 50000.32,
            "last_day_change": 4.0
        }
        self.data_test = CryptoCurrency.objects.create(**self.cryptocurrency_data)

    def test_get_coin_success(self):
        url = reverse('cryptocurrencies_detail', kwargs={"pk": self.data_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(result.get('name'), self.cryptocurrency_data.get('name'))

    def test_get_coin_fail(self):
        url = reverse('cryptocurrencies_detail', kwargs={"pk": self.data_test.id + 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        result = json.loads(response.content)
        self.assertNotEqual(result.get('name'), self.cryptocurrency_data.get('name'))

    def test_delete_coin(self):
        url = reverse('cryptocurrencies_detail', kwargs={"pk": self.data_test.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.content, b'')

    def test_update_coin_success(self):
        data_update = {
            "name": "Bitcoin4",
            "symbol": "BTC4",
            "logo": "https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
            "price": 39429.05,
            "last_day_change": 2.65
        }

        url = reverse('cryptocurrencies_detail', kwargs={"pk": self.data_test.id})
        response = self.client.put(url, data_update, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(result.get('name'), data_update.get('name'))

    def test_update_coin_fail(self):
        data_update = {
            "name": "TestCoin2"
        }

        url = reverse('cryptocurrencies_detail', kwargs={"pk": self.data_test.id})
        response = self.client.put(url, data_update, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        result = json.loads(response.content)
        self.assertNotEqual(result.get('name'), data_update.get('name'))
