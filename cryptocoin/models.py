from django.db import models


# Create your models here.

class TransactionType(models.Model):
    transaction_type = models.CharField(max_length=10)

    def __str__(self):
        return self.transaction_type


class CryptoCurrency(models.Model):
    name = models.CharField(max_length=30, unique=True)
    symbol = models.CharField(max_length=10, unique=True)
    logo = models.TextField()
    price = models.FloatField()
    last_day_change = models.FloatField()
    last_update = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Transaction(models.Model):
    transaction_type = models.ForeignKey(TransactionType, on_delete=models.CASCADE)
    cryptocurrency_symbol = models.ForeignKey(CryptoCurrency, on_delete=models.CASCADE)
    quantity = models.FloatField()
    coin_price = models.FloatField()

    def __str__(self):
        return f"{self.transaction_type} {self.cryptocurrency_symbol}"
