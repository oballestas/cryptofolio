## Dependencies

* [pipenv](https://pipenv.pypa.io/en/latest/install/#installing-pipenv)

## Installation

Everytime you pull the changes from this repository, you must execute the following commands:

1. `pipenv install`
2. `pipenv shell`
3. `python manage.py migrate`

Optionally, if you need to set up your IDE, you can use the command `which python` to get the environment's path.

## Running the project

The following commands must be executed in different terminals in order to run the project properly:

1. `python manage.py runserver`
2. `sudo docker run -p 5672:5672 --name cryptofolio rabbitmq:3`
3. `celery -A cryptofolio worker -l info`
4. `celery -A cryptofolio beat -l info`

## Testing

1. `coverage erase`
2. `coverage run manage.py test`
3. `coverage report`
4. `coverage html`
5. Open the `htmlcov/index.html` file report on your browser. 