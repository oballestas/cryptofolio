import json
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from cryptocoin.models import TransactionType


class TransactionTypeListTest(APITestCase):
    """
    Test for all the transactions types:
    -For the 'get' method.
    -For the 'post' method for successful and failed responses.
    """

    def setUp(self):
        self.transaction_type_data = {
            "transaction_type": "Buy",
        }

    def test_get_transaction_type(self):
        TransactionType.objects.create(**self.transaction_type_data)
        response = self.client.get(reverse('transaction_types'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(len(result), 1)

    def test_create_transaction_type_success(self):
        url = reverse('transaction_types')
        response = self.client.post(url, self.transaction_type_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        result = json.loads(response.content)
        self.assertEqual(result.get('transaction_type'), self.transaction_type_data.get('transaction_type'))

    def test_create_transaction_type_fail(self):
        url = reverse('transaction_types')
        fail_data = {}
        response = self.client.post(url, fail_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TransactionTypeDetailTest(APITestCase):
    """
    Test for a specific transaction type (detail view):
    -For the 'get' and 'put' methods for successful and failed responses.
    -For the 'delete' method.
    """

    def setUp(self):
        self.transaction_type_data = {
            "transaction_type": "Buy",
        }
        self.data_test = TransactionType.objects.create(**self.transaction_type_data)

    def test_get_transaction_type_success(self):
        url = reverse('transaction_types_detail', kwargs={"pk": self.data_test.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(result.get('transaction_type'), self.transaction_type_data.get('transaction_type'))

    def test_get_transaction_type_fail(self):
        url = reverse('transaction_types_detail', kwargs={"pk": self.data_test.id + 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_transaction_type(self):
        url = reverse('transaction_types_detail', kwargs={"pk": self.data_test.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.content, b'')

    def test_update_transaction_type_success(self):
        data_update = {
            "transaction_type": "Sell",
        }

        url = reverse('transaction_types_detail', kwargs={"pk": self.data_test.id})
        response = self.client.put(url, data_update, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(result.get('transaction_type'), data_update.get('transaction_type'))

    def test_update_transaction_type_fail(self):
        data_update = {
            "transaction": "Sell",
        }

        url = reverse('transaction_types_detail', kwargs={"pk": self.data_test.id})
        response = self.client.put(url, data_update, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
