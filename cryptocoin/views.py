from django.http import Http404, HttpResponseRedirect
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import TransactionType, CryptoCurrency, Transaction
from .serializers import TransactionTypeSerializer, CryptoCurrencySerializer, TransactionSerializer
from django.shortcuts import render
from .forms import AddTransaction


class TransactionTypeList(APIView):
    """
    List all transaction types, or create a new transaction type.
    """

    def get(self, request):
        transaction_types = TransactionType.objects.all()
        serializer = TransactionTypeSerializer(transaction_types, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = TransactionTypeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TransactionTypeDetail(APIView):
    """
    Retrieve, update or delete a transaction type instance.
    """

    def get_object(self, pk):
        try:
            return TransactionType.objects.get(pk=pk)
        except TransactionType.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        transaction_type = self.get_object(pk)
        serializer = TransactionTypeSerializer(transaction_type)
        return Response(serializer.data)

    def put(self, request, pk):
        transaction_type = self.get_object(pk)
        serializer = TransactionTypeSerializer(transaction_type, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        transaction_type = self.get_object(pk)
        transaction_type.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CryptoCurrencyList(APIView):
    """
    List all cryptocurrencies, or create a new cryptocurrency.
    """

    def get(self, request):
        cryptocurrencies = CryptoCurrency.objects.all()
        serializer = CryptoCurrencySerializer(cryptocurrencies, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CryptoCurrencySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CryptoCurrencyDetail(APIView):
    """
    Retrieve, update or delete a cryptocurrency instance.
    """

    def get_object(self, pk):
        try:
            return CryptoCurrency.objects.get(pk=pk)
        except CryptoCurrency.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        cryptocurrency = self.get_object(pk)
        serializer = CryptoCurrencySerializer(cryptocurrency)
        return Response(serializer.data)

    def put(self, request, pk):
        cryptocurrency = self.get_object(pk)
        serializer = CryptoCurrencySerializer(cryptocurrency, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        cryptocurrency = self.get_object(pk)
        cryptocurrency.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TransactionList(APIView):
    """
    List all transaction types, or create a new transaction type.
    """

    def get(self, request):
        transactions = Transaction.objects.all()
        serializer = TransactionSerializer(transactions, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TransactionDetail(APIView):
    """
    Retrieve, update or delete a transaction type instance.
    """

    def get_object(self, pk):
        try:
            return Transaction.objects.get(pk=pk)
        except Transaction.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        transaction = self.get_object(pk)
        serializer = TransactionSerializer(transaction)
        return Response(serializer.data)

    def put(self, request, pk):
        transaction = self.get_object(pk)
        serializer = TransactionSerializer(transaction, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        transaction = self.get_object(pk)
        transaction.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


def home(request):
    return render(request, 'home.html')


def portfolio(request):
    return render(request, 'portfolio.html')


def add_transaction(request):
    form = AddTransaction()

    if request.method == 'POST':
        form = AddTransaction(request.POST)

        if form.is_valid():
            transaction_info = {
                'transaction_type': form.cleaned_data["transaction_type"],
                'cryptocurrency_symbol': form.cleaned_data["cryptocurrency_symbol"],
                'quantity': form.cleaned_data["quantity"],
                'coin_price': form.cleaned_data["coin_price"],
            }
            transaction_ = Transaction(**transaction_info)
            transaction_.save()

            return HttpResponseRedirect('/portfolio')

    context = {
        'form': form
    }

    return render(request, 'add_transaction.html', context)
