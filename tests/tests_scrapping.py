from django.test import TestCase

from cryptocoin.tasks import update_prices


# Create your tests here.

class ScrappingTest(TestCase):

    def test_scrapping_data(self):
        response = update_prices()
        self.assertIsNotNone(response)

    def test_scrapping_data_celery(self):
        response = update_prices.delay()
        self.assertIsNotNone(response)
