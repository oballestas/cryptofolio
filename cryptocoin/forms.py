from django import forms
from .models import Transaction, TransactionType, CryptoCurrency


class AddTransaction(forms.Form):
    transaction_type = forms.ModelChoiceField(label="Transaction Type", queryset=TransactionType.objects.all(),
                                              required=True)
    cryptocurrency_symbol = forms.ModelChoiceField(label="Cryptocurrency", queryset=CryptoCurrency.objects.all(),
                                                   required=True)
    quantity = forms.FloatField(label="Quantity", required=True)
    coin_price = forms.FloatField(label="Price", required=True)
