from celery import shared_task
from cryptofolio.celery import app
from bs4 import BeautifulSoup
import requests
import re
from .models import CryptoCurrency


@app.task
def update_prices():
    url = "https://coinmarketcap.com/"
    result = requests.get(url).text
    doc = BeautifulSoup(result, "html.parser")
    tbody = doc.tbody
    trs = tbody.contents

    data_coins = []
    for tr in trs[:10]:
        name, price, last_day_price = tr.contents[2:5]
        fixed_name = name.p.string
        fixed_price = str(price.a.string).replace('$', '').replace(',', '')
        fixed_last_day_price_change = float(str(last_day_price.span).split('</span>')[-2].split('<')[0])
        change = last_day_price.span.find_all(class_=re.compile('icon-Caret'))[0]['class'][0]
        if 'down' in change:
            fixed_last_day_price_change *= -1
        symbol = name.find(class_='coin-item-symbol').string
        logo = name.img['src']

        crypto_info = {
            'name': fixed_name,
            'symbol': symbol,
            'logo': logo,
            'price': fixed_price,
            'last_day_change': fixed_last_day_price_change,
        }

        kwargs = {
            'symbol': symbol,
        }

        CryptoCurrency.objects.update_or_create(
            **kwargs, defaults=crypto_info
        )

        # data_coins.append(crypto_info)

        # print('obj:', obj)
        # print('created:', created)

    # print(data_coins)
    return data_coins
