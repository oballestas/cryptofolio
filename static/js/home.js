import {consumeAPI} from "./index.js";

async function showCryptocurrencies(pk = null) {

    let data = await consumeAPI('cryptocurrencies/', pk);

    let doc = document;
    let tbody = doc.getElementById('cryptocurrency-table-body');

    let cryptocurrency_list_html = ``;
    for (let i = 0; i < data.length; i++) {

        let crypto = data[i]

        cryptocurrency_list_html += `
            <tr>
                <td>${crypto.id}</td>
                <td><img class="crypto-logo" src="${crypto.logo}" alt="${crypto.name} logo"> ${crypto.name} <span class="crypto-symbol">${crypto.symbol}</span></td>
                <td>${crypto.price.toLocaleString()}</td>
                <td><span class="change-${crypto.last_day_change < 0 ? 'down' : 'up'}">$ ${crypto.last_day_change.toLocaleString()}</span></td>
            </tr>`;

    }

    tbody.innerHTML = cryptocurrency_list_html;

}

showCryptocurrencies();