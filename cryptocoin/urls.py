from django.urls import path
# from .views import portfolio, add_transaction

from .views import CryptoCurrencyList, CryptoCurrencyDetail, portfolio, add_transaction

from .views import TransactionTypeList, TransactionTypeDetail
from .views import TransactionList, TransactionDetail

urlpatterns = [
    path('add_transaction/', add_transaction, name='add_transaction'),
    path('transaction_types/', TransactionTypeList.as_view(), name="transaction_types"),
    path('transaction_types/<int:pk>/', TransactionTypeDetail.as_view(), name="transaction_types_detail"),
    path('cryptocurrencies/', CryptoCurrencyList.as_view(), name='cryptocurrencies_list'),
    path('cryptocurrencies/<int:pk>/', CryptoCurrencyDetail.as_view(), name='cryptocurrencies_detail'),
    path('transactions/', TransactionList.as_view(), name="transactions"),
    path('transactions/<int:pk>/', TransactionDetail.as_view(), name="transaction_detail"),
]
