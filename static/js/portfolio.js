import {consumeAPI} from "./index.js";

async function showTransactions(pk = null) {

    let cryptocurrencies = await consumeAPI('cryptocurrencies/');
    let transaction_types = await consumeAPI('transaction_types/');
    let transactions = await consumeAPI('transactions/', pk);

    let doc = document;
    let tbody = doc.getElementById('portfolio-table-body');
    let portfolio_list_html = ``;

    for (let i = 0; i < transactions.length; i++) {
        let transaction = transactions[i];
        let transaction_crypto_info = cryptocurrencies.filter(cryptocurrency => cryptocurrency.id === transaction.cryptocurrency_symbol)[0];
        let transaction_type_info = transaction_types.filter(transaction_type => transaction_type.id === transaction.transaction_type)[0];

        portfolio_list_html += `
            <tr>
                <td><img class="crypto-logo" src="${transaction_crypto_info.logo}" alt="${transaction_crypto_info.name} logo"> ${transaction_crypto_info.name} <span class="crypto-symbol">${transaction_crypto_info.symbol}</span></td>
                <td>${transaction_type_info.transaction_type}</td>
                <td>${transaction.quantity.toLocaleString()}</td>
                <td>$ ${transaction.coin_price.toLocaleString()}</td>
                <td>$ ${(transaction.quantity * transaction.coin_price).toLocaleString()}</td>
            </tr>`;

    }

    tbody.innerHTML = portfolio_list_html;

}

showTransactions();

