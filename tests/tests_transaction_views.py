import json

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from cryptocoin.models import Transaction, CryptoCurrency, TransactionType


class TransactionListTest(APITestCase):
    """
    Test for all the transactions:
    -For the 'get' method.
    -For the 'post' method for successful and failed responses.
    """

    def setUp(self):
        self.transaction_type_data = {
            "transaction_type": "Buy",
        }
        self.transaction_type_object = TransactionType.objects.create(**self.transaction_type_data)

        self.cryptocurrency_data = {
            "name": "TestCoin",
            "symbol": "TC",
            "logo": "https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
            "price": 50000.32,
            "last_day_change": 4.0
        }
        self.cryptocurrency_object = CryptoCurrency.objects.create(**self.cryptocurrency_data)

        self.transaction_data = {
            "transaction_type": self.transaction_type_object.id,
            "cryptocurrency_symbol": self.cryptocurrency_object.id,
            'quantity': 0.10,
            'coin_price': 50000,
        }

    def test_create_transaction_type_success(self):
        url = reverse('transactions')
        response = self.client.post(url, self.transaction_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        result = json.loads(response.content)
        self.assertEqual(result.get('transaction_type'), self.transaction_data.get('transaction_type'))

    def test_get_transaction_type(self):
        url = reverse('transactions')
        self.client.post(url, self.transaction_data, format='json')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(len(result), 1)

    def test_create_transaction_type_fail(self):
        url = reverse('transactions')
        fail_data = {}
        response = self.client.post(url, fail_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TransactionDetailTest(APITestCase):
    """
    Test for a specific transaction (detail view):
    -For the 'get' and 'put' methods for successful and failed responses.
    -For the 'delete' method.
    """

    def setUp(self):
        self.transaction_type = {
            "transaction_type": "Sell",
        }
        self.transaction_type_object = TransactionType.objects.create(**self.transaction_type)

        self.cryptocurrency = {
            "name": "TestCoin",
            "symbol": "TC",
            "logo": "https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
            "price": 50000.32,
            "last_day_change": 4.0
        }
        self.cryptocurrency_object = CryptoCurrency.objects.create(**self.cryptocurrency)

        self.transaction_data = {
            "transaction_type": self.transaction_type_object,
            "cryptocurrency_symbol": self.cryptocurrency_object,
            'quantity': 0.10,
            'coin_price': 50000,
        }
        self.transaction_object = Transaction.objects.create(**self.transaction_data)

    def test_get_transaction_success(self):
        url = reverse('transaction_detail', kwargs={"pk": self.transaction_object.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(result.get('transaction_type'), self.transaction_data.get('transaction_type').id)

    def test_get_transaction_fail(self):
        url = reverse('transaction_detail', kwargs={"pk": self.transaction_object.id + 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_transaction_type(self):
        url = reverse('transaction_detail', kwargs={"pk": self.transaction_object.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.content, b'')

    def test_update_transaction_success(self):
        data_update = {
            "transaction_type": self.transaction_type_object.id,
            "cryptocurrency_symbol": self.cryptocurrency_object.id,
            'quantity': 0.14,
            'coin_price': 54000,
        }

        url = reverse('transaction_detail', kwargs={"pk": self.transaction_object.id})
        response = self.client.put(url, data_update, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertEqual(result.get('transaction_type'), data_update.get('transaction_type'))

    def test_update_transaction_type_fail(self):
        data_update = {
            "transaction_type": self.transaction_type_object.id,
        }

        url = reverse('transaction_detail', kwargs={"pk": self.transaction_object.id})
        response = self.client.put(url, data_update, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
